﻿using System.Windows;

namespace _23._3___Werknemer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            List<Werknemer> werknemers = new List<Werknemer>();
            Werknemer werknemer = new Werknemer(txtAchternaam.Text, txtVoornaam.Text, Convert.ToDouble(txtVerdiensten.Text));
            werknemers.Add(werknemer);
            foreach (Werknemer werknemer1 in werknemers) 
            {
                lbDisplay.Items.Add(werknemer1.VolledigeWeergave);
            }
            txtAchternaam.Clear();
            txtVoornaam.Clear();
            txtVerdiensten.Clear();
        }

        private void Window_Loaded()
        {

        }
    }
}