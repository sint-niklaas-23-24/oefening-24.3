﻿namespace _23._3___Werknemer
{
    class Werknemer
    {
        //Attributen
        private string _naam;
        private string _voornaam;
        private double _verdiensten;
        //Constructors
        public Werknemer(string naam, string voornaam, double verdiensten)
        {
            Naam = naam;
            Voornaam = voornaam;
            Verdiensten = verdiensten;
        }
        //properties
        public string Naam
        {
            get { return _naam; }
            set { _naam = value; }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set { _voornaam = value; }
        }
        public double Verdiensten
        {
            get { return _verdiensten; }
            set { _verdiensten = value; }
        }
        public string VolledigeWeergave
        {
            get { return Naam.PadRight(25) + Voornaam.PadRight(25) + "€" + Verdiensten + Environment.NewLine; }
        }
        //methodes
    }
}
